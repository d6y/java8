package code;

import java.util.Map;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

public class EStream3 {
  public static void main(String[] args) {

    // flatMap and Collectors

    Stream<Movie> movies = Stream.of(
        new Movie("Godzilla", 1954),
        new Movie("Godzilla Raids Again", 1955, "Anguirus"),
        new Movie("King Kong vs. Godzilla", 1962, "King Kong", "Daidako"),
        new Movie("Mothra vs. Godzilla", 1964, "Mothra"),
        new Movie("Ghidorah, the Three-Headed Monster", 1964, "King Ghidorah", "Mothra", "Rodan"),
        new Movie("Invasion of Astro-Monster", 1965, "King Ghidorah", "Rodan"),
        new Movie("Godzilla vs. the Sea Monster", 1966, "Ebirah", "Mothra", "Ookondoru"),
        new Movie("Son of Godzilla", 1967, "Kamacuras", "Kumonga", "Minilla"),
        new Movie("Destroy All Monsters", 1968, "Anguirus", "Baragon", "Gorosaurus", "King Ghidorah", "Kumonga", "Manda", "Minilla", "Mothra", "Rodan", "Varan"),
        new Movie("All Monsters Attack", 1969, "Gabara", "Minilla", "Maneater"),
        new Movie("Godzilla vs. Hedorah", 1971, "Hedorah"),
        new Movie("Godzilla vs. Gigan", 1972, "Anguirus", "Gigan", "King Ghidorah"),
        new Movie("Godzilla vs. Megalon", 1973, "Gigan", "Jet Jaguar", "Megalon"),
        new Movie("Godzilla vs. Mechagodzilla", 1974, "Anguirus", "King Caesar", "Mechagodzilla"),
        new Movie("Terror of Mechagodzilla", 1975, "Mechagodzilla", "Titanosaurus")
    );

    //movies.flatMap(m -> m.coStars).forEach(System.out::println);

    // Collectors - a kind of super reduce?
    // Supplier, Accumulator, Combiner, and Finisher.

    movies.flatMap(m -> m.coStars)
        .collect(toSet()).forEach(System.out::println);


   Map<String,Long> popular =
       movies.
       flatMap(m -> m.coStars).
       collect(groupingBy(s -> s, counting()));

   popular.forEach( (k,v) -> System.out.println("Key "+k+" = "+v));



   //  Map<Boolean,List<Movie>> r = movies.collect(Collectors.partitioningBy(m -> m.year < 1970));
   // r.forEach( (b,mvs) -> System.out.println(b + " = "+mvs));


  }
}
