package code;

public class C3Fun {

  public static void main(String[] args) {

    // More than 2 arguments

    TriFunction<Integer,Integer,Integer,String> tri =
        (x,y,z) -> "ok";

    System.out.println(  tri.apply(1,2,3) );
  }


}
