package code;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.UnaryOperator;

public class B2Type {

  public static void main(String[] args) {

    // New functional interfaces java.util.function

    UnaryOperator<Integer> treble = x -> x * 3;
    // How to apply it?
    Integer result = treble.apply(7);

    BinaryOperator<Integer> add = (a,b) -> a + b;
    Integer three = add.apply(1,2);

    LocalDate today = LocalDate.now();
    Function<LocalDate, String> tomorrow = d -> d.plusDays(1).getDayOfWeek().name();

    // Is UnaryOperator<Integer> a Function<Integer,Integer> ?

    BiFunction<LocalDate, LocalDate, Long> daysBetween =
        (d1, d2) -> d1.until(d2, ChronoUnit.DAYS);

    // Is BinaryOperator<Integer> a BiFunction<Integer,Integer,Integer> ?

    Double excitementLevel = 1d / daysBetween.apply(LocalDate.now(), LocalDate.of(2014,12,25));
    System.out.println(excitementLevel);


  }
}
