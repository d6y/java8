package code;

import java.util.function.Function;

public class C1Fun {

  public static void main(String[] args) {

    // What else can we do with a function?

    // Higher-order: functions that return functions
    Function<Integer, Function<Character,String>> f;








    // Repeating characters example:
    Function<Integer, Function<Character,String>> repeat = x ->
        c -> String.format("%0" + x + "d", 0).replace("0",c.toString());


    Function<Character,String> ten = repeat.apply(10);

    System.out.println( ten.apply('*') );
    System.out.println( ten.apply('.') );
    System.out.println( ten.apply('-') );


  }


}
