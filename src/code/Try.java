package code;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

public abstract class Try<T> {

  public static class Success<T> extends Try<T> {
    private final T value;

    public Success(T t) {
      this.value = t;
    }

    @Override
    public String toString() {
      return "Success("+value+")";
    }

    @Override
    public boolean isSuccess() {
      return true;
    }

    @Override
    public T get() {
      return value;
    }

    @Override
    public T getOr(T defaultValue) {
      return value;
    }

    @Override
    public T getOrElse(Supplier<T> defaultSupplier) {
      return value;
    }

  }

  public static class Failure<T> extends Try<T> {
    private final Throwable value;

    public Failure(Throwable t) {
      this.value = t;
    }

    @Override
    public String toString() {
      return "Failure("+value.getMessage()+")";
    }

    @Override
    public boolean isSuccess() {
      return false;
    }

    @Override
    public T get() {
      throw new IllegalStateException("Cannot call get on a Failure", value);
    }

    @Override
    public T getOr(T defaultValue) {
      return defaultValue;
    }

    @Override
    public T getOrElse(Supplier<T> defaultSupplier) {
      return defaultSupplier.get();
    }

  }

  public abstract boolean isSuccess();

  public boolean isFailure() {
    return !isSuccess();
  }

  public abstract T get();
  public abstract T getOr(T alternative);
  public abstract T getOrElse(Supplier<T> alternative);

  public static <T> Try<T> to(Supplier<T> s) {
    try {
      return new Success(s.get());
    } catch (Throwable ex) {
      return new Failure(ex);
    }
  }

  public <U> Try<U> map(Function<? super T, ? extends U> f) {
    if (isSuccess()) return Try.to(() -> f.apply(get())); else return (Try<U>)this;
  }


  public static void main(String[] args) {

    Try<Integer> t1 = Try.to( () -> Integer.parseInt("Hello") ).map(x -> x * 10);
    System.out.println(t1);
    System.out.println(1 + t1.getOr(100));
    System.out.println(1 + t1.getOrElse(() -> 1000));

    Try<Integer> t2 = Try.to( () -> Integer.parseInt("4") ).map( x -> x * 10);
    System.out.println(t2);

    Try<String> t3 = Try.to( () -> Integer.parseInt("4") ).map( x -> "The number is "+x );
    System.out.println(t3);

    Stream.of(t1,t2,t3).map(t -> t.isFailure()).forEach(System.out::println);
  }


}
