package code;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EStream2 {
  public static void main(String[] args) {

    Stream<Integer> positives = Stream.iterate(0, x -> x + 1);

    // All the evens?  Actually, maybe not _all_ of them....
    //positives.filter(x -> x % 2 == 0).limit(10).forEach(System.out::println);


    Map<String, List<Integer>> work =
        positives.parallel().
        limit(100).
        collect(Collectors.groupingBy(num -> Thread.currentThread().getName()));

    work.forEach( (k,v) -> System.out.println(k+" "+v));



  }
}
