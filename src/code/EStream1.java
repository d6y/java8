package code;

import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class EStream1 {
  public static void main(String[] args) {

    Stream<Movie> movies = Stream.of(
        new Movie("Godzilla", 1954),
        new Movie("Godzilla Raids Again", 1955),
        new Movie("King Kong vs. Godzilla", 1962),
        new Movie("Mothra vs. Godzilla", 1964),
        new Movie("Ghidorah, the Three-Headed Monster", 1964),
        new Movie("Invasion of Astro-Monster", 1965),
        new Movie("Godzilla vs. the Sea Monster", 1966),
        new Movie("Son of Godzilla", 1967),
        new Movie("Destroy All Monsters", 1968),
        new Movie("All Monsters Attack", 1969),
        new Movie("Godzilla vs. Hedorah", 1971),
        new Movie("Godzilla vs. Gigan", 1972),
        new Movie("Godzilla vs. Megalon", 1973),
        new Movie("Godzilla vs. Mechagodzilla", 1974),
        new Movie("Terror of Mechagodzilla", 1975)
     ); // ...there are more...


    // Source -> intermediate operations -> terminal operations
    //           lazy                       eager
    //           stateful v stateless

   // Stream<Integer> years = movies.map(m -> m.year).peek(System.out::println);

    // Reduce is a dead handy tool.
   // Integer sumOfYears = years.reduce(0, (a, y) -> a + y);


    // Convert early films (pre-1970s) to a list via collect:

    Function<Movie,Integer> yf = m -> m.year;
    Predicate<Integer> isEarly = y -> y < 1970;

    Stream<Integer> early = movies.
        map(yf).
        filter(isEarly);




  }
}
