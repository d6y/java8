package code;

import java.util.Optional;

public class FOption1 {
  public static void main(String[] args) {

    Optional<Integer> o = Optional.ofNullable(7);
    Optional<Integer> e = Optional.empty();
    Optional<Integer> n = Optional.ofNullable(null);

    // Don't use get.  You want to map, orElse or orElseGet(f)

    // Or test with isPresent()

  }
}
