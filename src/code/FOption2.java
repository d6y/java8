package code;

import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

public class FOption2 {
  public static void main(String[] args) {

    Optional<String> input = Optional.ofNullable("kjsdjdsfghjdfsdfsjgh107");

    Function<String, Optional<Integer>> parseInt = s -> {
      try {
        return Optional.ofNullable(Integer.parseInt(s));
      } catch (NumberFormatException nfx) {
        return Optional.empty();
      }
     };

    // Parse that input or default to 21
    System.out.println( input.flatMap(parseInt).orElse(21) );


    // But, why not extend the language if you do this a lot...


    Try<Integer> nextInt = Try.to( () -> Integer.parseInt("hello") ).map(x -> x + 1);

  }
}
