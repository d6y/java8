package code;

import java.util.concurrent.*;

public class ASyntax {

  public static void main(String[] args) {

    final ExecutorService service =
        Executors.newFixedThreadPool(4);

    service.execute(new Runnable() {
      public void run() {
        System.out.println("I am running");
      }
    });


    service.execute( () -> System.out.println("I am also running") );

    service.shutdown();

  }
}
