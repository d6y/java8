package code;

import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class B3Type {

  public static void main(String[] args) {

    // Invoking is not uniform...

    Predicate<Integer> biggerThanThree = x -> x > 3;

    // Is Predicate<T> a Function<T,Boolean> ?



    Supplier<Integer> rand = () -> new Random().nextInt();
    Integer someInt = rand.get();

    Consumer<Integer> printNum = x -> System.out.println(x);
    printNum.accept(20);

    // Function references:
    Consumer<Integer> ref = System.out::println;
    ref.accept(20);

    // There are more...

  }
}
