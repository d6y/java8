package code;

import java.util.function.Function;

public class DeSugar {

  Function<Integer, Function<Long,String>> f = i -> l -> "hello";

}


//code$ java -jar cfr_0_79.jar DeSugar.class --decodelambdas false
///*
// * Decompiled with CFR 0_79.
// *
// * Could not load the following classes:
// *  java.lang.invoke.LambdaMetafactory
// *  java.util.function.Function
// */
//    package code;
//
//    import java.util.function.Function;
//
//public class DeSugar {
//  Function<Integer, Function<Long, String>> f = (Function)LambdaMetafactory.metafactory(null, null, null, (Ljava/lang/Object;)Ljava/lang/Object;, lambda$new$1(java.lang.Integer ), (Ljava/lang/Integer;)Ljava/util/function/Function;)();
//
//  private static /* synthetic */ Function lambda$new$1(Integer i) {
//    return (Function)LambdaMetafactory.metafactory(null, null, null, (Ljava/lang/Object;)Ljava/lang/Object;, lambda$null$0(java.lang.Long ), (Ljava/lang/Long;)Ljava/lang/String;)();
//  }
//
//  private static /* synthetic */ String lambda$null$0(Long l) {
//    return "hello";
//  }
//}
