package code;

import java.awt.event.ActionListener;
import java.util.Comparator;

public class B1Type {

  public static void main(String[] args) {

    // "Functional Interfaces"
    // ... have exactly one abstract method

    Runnable r = () -> System.out.println("Running");

    ActionListener l = event -> System.out.println(event.getID());

    // Note: expressions vs. statements
    Comparator<Integer> c = (x,y) -> {
      if (x == y) return 0; else if (x >= y) return 1; else return -1;
    };

    // But they seems a bit... specific?

  }
}
