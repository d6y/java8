package code;

import java.util.stream.Stream;

public class Movie {

  public final String title;
  public final Integer year;
  public final Stream<String> coStars;

  public Movie(final String title, final Integer year, final String... coStars) {
    this.title = title;
    this.year = year;
    this.coStars = Stream.of(coStars);
  }

  public Movie(final String title, final Integer year) {
    this(title, year, new String[0]);
  }

  public String getTitle() {
    return this.title;
  }

  @Override
  public String toString() {
    return "Movie(" +
        "title='" + title + '\'' +
        ", year=" + year +
        //", coStars=" + coStars +
        ')';
  }
}