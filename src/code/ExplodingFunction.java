package code;

import java.io.IOException;

@FunctionalInterface
interface ExplodingFunction<T,R> {

  T apply(R r) throws IOException;

}
