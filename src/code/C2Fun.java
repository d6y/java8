package code;

import java.util.function.Function;

public class C2Fun {

  public static void main(String[] args) {

    // Composing functions

    Function<Integer, Boolean> isEven = i -> i % 2 == 0;
    Function<Boolean, Boolean> not = b -> !b;

    Function<Integer, Boolean> isOdd = not.compose(isEven);

    // Or if you don't like that...
    Function<Integer, Boolean> isOddAlt = isEven.andThen(not);


  }
}
