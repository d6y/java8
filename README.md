# Functions and Streams in Java 8
## Exploration at [Functional Brighton](http://www.meetup.com/Functional-Brighton/) and [Java Brighton](http://www.meetup.com/Brighton-Java) June 2014 


In which we will look at...

* Motivation and syntax

* Various function types

* Streams



## Useful resources

* [Java 8 Stream package docs](http://docs.oracle.com/javase/8/docs/api/java/util/stream/package-summary.html)

* [State of the Lambda](http://cr.openjdk.java.net/~briangoetz/lambda/lambda-state-final.html)

* [Java SE 8 for the Really Impatient](http://horstmann.com/java8/) - Addison-Wesley

* [Java 8 Lambdas](http://shop.oreilly.com/product/0636920030713.do) - O'Reilly.

* [Lambdas in Java: A peek under the hood](http://www.youtube.com/watch?v=MLksirK9nnE) (video).

* [Type Inference in Java SE 8](http://www.parleys.com/play/5254689ae4b0c4f11ec576f9/about) (video).



